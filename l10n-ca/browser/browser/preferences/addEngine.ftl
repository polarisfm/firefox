# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

add-engine-button = Afegeix un motor personalitzat
add-engine-name = Nom del motor de cerca
add-engine-alias = Àlies
add-engine-cancel =
    .label = Cancel·la
    .accesskey = C
