# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxa-toolbar-sync-syncing =
    .label = Nagsi-Sync…
fxa-toolbar-sync-syncing-tabs =
    .label = Nagsi-sync ng mga Tab…
sync-disconnect-dialog-title = I-disconnect ang { -sync-brand-short-name }?
sync-disconnect-dialog-body = Hindi na magsi-sync ang { -brand-product-name } sa account mo pero hindi nito buburahin ang kahit anong browsing data sa device na ito.
fxa-disconnect-dialog-title = I-disconnect ang { -brand-product-name }?
fxa-disconnect-dialog-body = Magdi-disconnect ang { -brand-product-name } sa account mo pero hindi nito buburahin ang kahit anong browsing data sa device na ito.
sync-disconnect-dialog-button = Mag-disconnect
fxa-signout-dialog-heading = Mag-sign out sa { -fxaccount-brand-name }?
fxa-signout-dialog-body = Mananatili ang naka-sync na data sa iyong account.
fxa-signout-checkbox =
    .label = Burahin ang data sa device na ito (mga login, password, kasaysayan, bookmark, atbp.).
fxa-signout-dialog =
    .title = Mag-sign out sa { -fxaccount-brand-name }
    .style = min-width: 375px;
    .buttonlabelaccept = Mag-sign out
