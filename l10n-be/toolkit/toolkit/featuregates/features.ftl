# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-masonry2 =
    .label = CSS: выклад «Masonry»
experimental-features-css-masonry-description = Актывуе падтрымку эксперыментальнай функцыі CSS Masonry Layout. Зазірніце ў <a data-l10n-name="explainer"> тлумачальнік </a> для атрымання падрабязнага апісання функцыі. Каб пакінуць водгук, каментуйце ў <a data-l10n-name="w3c-issue"> гэтай тэме на GitHub </a> альбо <a data-l10n-name="bug"> ў гэтым спісе хібаў </a>.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-gpu2 =
    .label = Web API: WebGPU
experimental-features-web-gpu-description2 = Гэты новы API забяспечвае нізкаўзроўневую падтрымку для вылічэння і візуалізацыі графікі з дапамогаю <a data-l10n-name="wikipedia">графічнага працэсара (GPU)</a> прылады альбо камп'ютара карыстальніка. <a data-l10n-name="spec">Спецыфікацыя</a> знаходзіцца яшчэ ў распрацоўцы. Гл. <a data-l10n-name="bugzilla">хібу 1602129</a> для больш падрабязнай інфармацыі.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-media-avif =
    .label = Media: AVIF
experimental-features-media-avif-description = Калі гэтая функцыя ўключана, { -brand-short-name } будзе падтрымлiваць фармат файла выявы AV1 (AVIF). Гэта фармат файлаў нерухомых выяў, які выкарыстоўвае магчымасці алгарытмаў сціску відэа AV1 для памяншэння памеру выявы. Для больш падрабязнай інфармацыі гл. <a data-l10n-name="bugzilla">хiбу 1443863</a>.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-inputmode =
    .label = Web API: inputmode
# "inputmode" and "contenteditable" are technical terms and shouldn't be translated.
experimental-features-web-api-inputmode-description = Наша рэалізацыя глабальнага атрыбута <a data-l10n-name="mdn-inputmode">inputmode</a> была абноўлена ў адпаведнасці з <a data-l10n-name="whatwg">спецыфікацыяй WHATWG</a>, але нам трэба яшчэ ўнесці іншыя змены, напрыклад, зрабіць яго даступным для змесціва contenteditable. Для больш падрабязнай інфармацыі гл. <a data-l10n-name="bugzilla">хiбу 1205133</a>.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-link-preload =
    .label = Web API: <link rel="preload">
# Do not translate "rel", "preload" or "link" here, as they are all HTML spec
# values that do not get translated.
experimental-features-web-api-link-preload-description = Атрыбут <a data-l10n-name="rel">rel</a> са значэннем <code>"preload"</code> на элеменце <a data-l10n-name="link">&lt;link&gt;< /a> прызначаны для павышэння прадукцыйнасці, дазваляючы загружаць рэсурсы на ранейшых этапах жыццёвага цыкла старонкі, гарантуючы, што яны будуць даступныя раней і менш верагодна будуць блакаваць рэндэрынг старонкі. Прачытайце <a data-l10n-name="readmore">“Папярэдняя загрузка змесціва з дапамогай <code>rel ="preload"</code>”</a> альбо гл. <a data-l10n-name="bugzilla">хiбу 1583604</a> для больш падрабязнай інфармацыі.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-focus-visible =
    .label = CSS: Pseudo-class: :focus-visible
experimental-features-css-focus-visible-description = Дазваляе ўжываць стылі фокусу да такіх элементаў, як кнопкі і элементы кіравання формай, толькі тады, калі яны факусуюцца з дапамогай клавіятуры (напрыклад, пры пераключэннi паміж элементамі), а не тады, калі яны факусуюцца з дапамогай мышы ці іншай паказальнай прылады. Для больш падрабязнай інфармацыі гл. <a data-l10n-name="bugzilla">хiбу 1617600</a>.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-beforeinput =
    .label = Web API: beforeinput Event
# The terms "beforeinput", "input", "textarea", and "contenteditable" are technical terms
# and shouldn't be translated.
experimental-features-web-api-beforeinput-description = Глабальная падзея <a data-l10n-name="mdn-beforeinput">beforeinput</a> запускаецца на <a data-l10n-name="mdn-input">&lt;input&gt;</a> і < data-l10n-name = "mdn-textarea">&lt;textarea&gt;</a> элементах, альбо на любых элементах з уключаным атрыбутам <a data-l10n-name="mdn-contenteditable">contenteditable</a>, непасрэдна перад зменай значэння элемента. Падзея дазваляе вэб-праграмам перавызначыць прадвызначаныя паводзіны браўзера пры ўзаемадзеяннi з карыстальнiкам, напрыклад, вэб-праграмы могуць адмяніць увод карыстальніка толькі для пэўных сімвалаў альбо могуць змяніць устаўку стылізаванага тэксту толькі з зацверджанымi стылямі.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-constructable-stylesheets =
    .label = CSS: Constructable Stylesheets
experimental-features-css-constructable-stylesheets-description = Даданне канструктара ў інтэрфейс <a data-l10n-name="mdn-cssstylesheet">CSSStyleSheet</a>, а таксама мноства звязаных з гэтым змяненняў дазваляе наўпрост ствараць новыя табліцы стыляў без неабходнасці дадаваць iх у HTML. Гэта значна палягчае стварэнне табліц стыляў для шматразовага выкарыстання з <a data-l10n-name="mdn-shadowdom">Shadow DOM</a>. Для больш падрабязнай інфармацыі гл. <a data-l10n-name="bugzilla">хiбу 1520690</a>.
experimental-features-devtools-color-scheme-simulation =
    .label = Інструменты распрацоўшчыка: Сімуляцыя колеравай схемы
experimental-features-devtools-color-scheme-simulation-description = Дадае магчымасць iмiтаваць розныя колеравыя схемы, што дазваляе праверыць медыя-запыты <a data-l10n-name="mdn-preferscolorscheme">@prefers-color-scheme</a>. Выкарыстанне гэтага медыя-запыту дазваляе вашай табліцы стыляў рэагаваць на тое, цi аддае карыстальнік перавагу светламу ці цёмнаму інтэрфейсу. Гэтая функцыя дазваляе праверыць код без неабходнасці змяняць налады браўзера (альбо аперацыйнай сістэмы, калі браўзер выкарыстоўвае сістэмную наладу колеравай схемы). Для больш падрабязнай iнфармацыi гл. <a data-l10n-name="bugzilla1">хiбу 1550804</a> і <a data-l10n-name="bugzilla2">хiбу 1137699</a>.
experimental-features-devtools-execution-context-selector =
    .label = Інструменты распрацоўшчыка: Выбар кантэксту выканання
experimental-features-devtools-execution-context-selector-description = Гэтая функцыя паказвае кнопку ў камандным радку кансолі, якая дазваляе змяніць кантэкст, у якім будзе выконвацца ўведзены выраз. Для больш падрабязнай iнфармацыi гл. <a data-l10n-name="bugzilla1">хiбу 1605154</a> і <a data-l10n-name="bugzilla2">хiбу 1605153</a>.
experimental-features-devtools-compatibility-panel =
    .label = Інструменты распрацоўніка: Панэль сумяшчальнасці
experimental-features-devtools-compatibility-panel-description = Бакавая панэль для карткi Інспектар, якая паказвае інфармацыю пра стан сумяшчальнасці вашай праграмы з рознымi браўзерамi. Для больш падрабязнай інфармацыі гл. <a data-l10n-name="bugzilla">хiбу 1584464</a>.
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-lax-by-default2 =
    .label = Кукі: SameSite=Lax прадвызначана
experimental-features-cookie-samesite-lax-by-default2-description = Тыпова трактаваць кукi як "SameSite=Lax", калі не вызначаны атрыбут "SameSite". Каб працаваць у рэжыме бягучага статус-кво неабмежаванага выкарыстання, распрацоўшчыкі павінны відавочна зацвердзiць "SameSite=None".
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-none-requires-secure2 =
    .label = Кукі: SameSite=None патрабуе атрыбута «secure»
experimental-features-cookie-samesite-none-requires-secure2-description = Кукi з атрыбутам "SameSite=None" патрабуюць засцярожанага атрыбута. Для гэтай функцыі патрабуецца "Кукi: SameSite=Lax прадвызначана".
# about:home should be kept in English, as it refers to the the URI for
# the internal default home page.
experimental-features-abouthome-startup-cache =
    .label = Кэш запуску about:home
experimental-features-abouthome-startup-cache-description = Кэш для пачатковага дакумента about:home, які прадвызначана загружаецца пры запуску. Мэта кэша - палепшыць прадукцыйнасць запуску.
experimental-features-print-preview-tab-modal =
    .label = Перапрацаванае акно друку
experimental-features-print-preview-tab-modal-description = Уводзiць перапрацаваны папярэдні прагляд друку і робіць яго даступным на macOS. Гэта патэнцыйна можа прывесцi да памылак і ўключае не ўсе налады, звязаныя з друкам. Каб атрымаць доступ да ўсіх налад, звязаных з друкам, выберыце "Друк з дапамогай сістэмнага дыялогу..." на панэлі "Друк".
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-cookie-samesite-schemeful =
    .label = Кукі: Schemeful SameSite
experimental-features-cookie-samesite-schemeful-description = Трактаваць кукi з аднаго дамена, але з рознымі схемамі (напрыклад, http://example.com і https://example.com), як мiжсайтавыя, а не з аднаго сайта. Паляпшае бяспеку, але можа прывесцi да памылак.
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support =
    .label = Інструменты распрацоўшчыка: Адладка Service Worker
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support-description = Актывуе эксперыментальную падтрымку Service Workers на панэлі Адладчыка. Гэтая функцыя можа запаволіць інструменты распрацоўшчыка і павялічыць спажыванне памяці.
# Desktop zooming experiment
experimental-features-graphics-desktop-zooming =
    .label = Графіка: плаўнае маштабаванне шчыпком
experimental-features-graphics-desktop-zooming-description = Актываваць падтрымку плаўнага маштабавання шчыпком для дотыкавых экранаў і прэцызійных дотыкавых панэлей.
# WebRTC global mute toggle controls
experimental-features-webrtc-global-mute-toggles =
    .label = Глабальнае адключэнне гуку WebRTC
experimental-features-webrtc-global-mute-toggles-description = Дадае элементы кіравання ў глабальны індыкатар сумеснага доступу WebRTC, якія дазваляюць карыстальнікам глабальна адключыць мікрафон і каналы камеры.
# JS JIT Warp project
experimental-features-js-warp =
    .label = JavaScript JIT: Warp
experimental-features-js-warp-description = Актываваць Warp, праект па паляпшэнні хуткасці JavaScript і памяншэнні выкарыстанай памяці.
# Fission is the name of the feature and should not be translated.
experimental-features-fission =
    .label = Fission (iзаляцыя сайта)
experimental-features-fission-description = Fission (ізаляцыя сайта) - гэта эксперыментальная магчымасць ў { -brand-short-name }, якая забяспечвае дадатковы ўзровень абароны ад хiбаў бяспекі. Выдзяляючы кожны сайт у асобны працэс, Fission ускладняе шкоднасным сайтам доступ да інфармацыі з іншых старонак, якія вы наведваеце. Гэта значнае архітэктурнае змяненне ў { -brand-short-name }, і мы будзем ўдзячныя вам за тэставанне і паведамленнi пра любыя праблемы, з якімi вы можаце сутыкнуцца. Для больш падрабязнай інфармацыі гл. <a data-l10n-name="wiki">вiкi</a>.
