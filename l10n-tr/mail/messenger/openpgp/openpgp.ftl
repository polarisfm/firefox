# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

e2e-intro-description = Şifrelenmiş veya dijital olarak imzalanmış iletiler göndermek için OpenPGP veya S/MIME gibi bir şifreleme teknolojisini yapılandırmanız gerekir.
e2e-intro-description-more = OpenPGP kullanımını etkinleştirmek için kişisel anahtarınızı veya S/MIME kullanımını etkinleştirmek için kişisel sertifikanızı seçin. Kişisel anahtar veya sertifikanın gizli anahtarına da sahip olmalısınız.
openpgp-key-user-id-label = Hesap / Kullanıcı kimliği
openpgp-keygen-title-label =
    .title = OpenPGP Anahtarı Oluştur
openpgp-cancel-key =
    .label = İptal
    .tooltiptext = Anahtar oluşturmayı iptal et
openpgp-key-gen-expiry-title =
    .label = Anahtar süre sonu
openpgp-key-gen-expire-label = Anahtarın süre soru:
openpgp-key-gen-days-label =
    .label = gün
openpgp-key-gen-months-label =
    .label = ay
openpgp-key-gen-years-label =
    .label = yıl
openpgp-key-gen-no-expiry-label =
    .label = Anahtarın süresi dolmasın
openpgp-key-gen-key-size-label = Anahtar boyutu
openpgp-key-gen-console-label = Anahtar üretimi
openpgp-key-gen-key-type-label = Anahtar türü
openpgp-key-gen-key-type-rsa =
    .label = RSA
openpgp-key-gen-key-type-ecc =
    .label = ECC (Eliptik Eğri)
openpgp-generate-key =
    .label = Anahtar oluştur
    .tooltiptext = Şifreleme ve/veya imzalama için yeni bir OpenPGP uyumlu anahtar oluşturur
openpgp-advanced-prefs-button-label =
    .label = Gelişmiş…
openpgp-key-expiry-label =
    .label = Süre sonu
openpgp-key-id-label =
    .label = Anahtar kimliği
openpgp-cannot-change-expiry = Bu karmaşık yapıya sahip bir anahtar. Son kullanma tarihinin değiştirilmesi desteklenmiyor.
openpgp-key-man-title =
    .title = OpenPGP Anahtar Yöneticisi
openpgp-key-man-generate =
    .label = Yeni anahtar çifti
    .accesskey = a
openpgp-key-man-gen-revoke =
    .label = İptal sertifikası
    .accesskey = İ
openpgp-key-man-ctx-gen-revoke-label =
    .label = İptal sertifikası oluştur ve kaydet
openpgp-key-man-file-menu =
    .label = Dosya
    .accesskey = D
openpgp-key-man-edit-menu =
    .label = Düzen
    .accesskey = z
openpgp-key-man-view-menu =
    .label = Görünüm
    .accesskey = G
openpgp-key-man-generate-menu =
    .label = Oluştur
    .accesskey = O
openpgp-key-man-keyserver-menu =
    .label = Anahtar sunucusu
    .accesskey = A
openpgp-key-man-import-public-from-file =
    .label = Ortak anahtarları dosyadan içe aktar
    .accesskey = O
openpgp-key-man-import-secret-from-file =
    .label = Gizli anahtar(lar)ı dosyadan içe aktar
openpgp-key-man-import-sig-from-file =
    .label = İptal(ler)i dosyadan içe aktar
openpgp-key-man-import-from-clipbrd =
    .label = Anahtar(lar)ı panodan içe aktar
    .accesskey = n
openpgp-key-man-import-from-url =
    .label = Anahtarları URL’den içe aktar
    .accesskey = U
openpgp-key-man-send-keys =
    .label = Ortak anahtarları e-postayla gönder
    .accesskey = e
openpgp-key-man-discover-progress = Aranıyor…
openpgp-key-copy-key =
    .label = Ortak anahtarı kopyala
    .accesskey = k
openpgp-key-send-key =
    .label = Ortak anahtarı e-postayla gönder
    .accesskey = ö
openpgp-key-man-ctx-copy-to-clipbrd-label =
    .label = Ortak anahtarları panoya kopyala
openpgp-key-man-close =
    .label = Kapat
openpgp-key-man-reload =
    .label = Anahtar önbelleğini yeniden yükle
    .accesskey = ö
openpgp-key-man-del-key =
    .label = Anahtar(lar)ı sil
    .accesskey = S
openpgp-delete-key =
    .label = Anahtarı sil
    .accesskey = S
openpgp-key-man-revoke-key =
    .label = Anahtarı iptal et
    .accesskey = i
openpgp-key-man-key-props =
    .label = Anahtar özellikleri
    .accesskey = ö
openpgp-key-man-key-more =
    .label = Daha fazla
    .accesskey = f
openpgp-key-man-view-photo =
    .label = Fotoğraflı kimlik
    .accesskey = F
openpgp-key-man-show-invalid-keys =
    .label = Geçersiz anahtarları göster
    .accesskey = g
openpgp-key-man-user-id-label =
    .label = Adı
openpgp-key-man-fingerprint-label =
    .label = Parmak izi
openpgp-key-man-select-all =
    .label = Tüm anahtarları seç
    .accesskey = T
openpgp-key-man-empty-tree-tooltip =
    .label = Yukarıdaki kutuya aranacak terimleri yazın
openpgp-key-man-nothing-found-tooltip =
    .label = Arama terimlerinizle eşleşen anahtar yok
openpgp-key-man-please-wait-tooltip =
    .label = Anahtarlar yüklenirken lütfen bekleyin…
openpgp-key-man-filter-label =
    .placeholder = Anahtar ara
openpgp-key-details-title =
    .title = Anahtar özellikleri
openpgp-key-details-signatures-tab =
    .label = Sertifikalar
openpgp-key-details-structure-tab =
    .label = Yapı
openpgp-key-details-id-label =
    .label = Kimlik
openpgp-key-details-key-type-label = Türü
openpgp-key-details-algorithm-label =
    .label = Algoritma
openpgp-key-details-size-label =
    .label = Boyut
openpgp-key-details-created-label =
    .label = Oluşturma
openpgp-key-details-created-header = Oluşturma
openpgp-key-details-expiry-label =
    .label = Süre sonu
openpgp-key-details-expiry-header = Süre sonu
openpgp-key-details-usage-label =
    .label = Kullanım
openpgp-key-details-fingerprint-label = Parmak izi
openpgp-key-details-sel-action =
    .label = Eylem seçin…
    .accesskey = E
openpgp-card-details-close-window-label =
    .buttonlabelaccept = Kapat
openpgp-acceptance-label =
    .label = Kabul durumunuz
openpgp-acceptance-rejected-label =
    .label = Hayır, bu anahtarı reddet.
openpgp-acceptance-undecided-label =
    .label = Henüz değil, belki sonra.
openpgp-acceptance-unverified-label =
    .label = Evet, ama bunun doğru anahtar olduğunu doğrulamadım.
openpgp-acceptance-verified-label =
    .label = Evet, bu anahtarın doğru parmak izine sahip olduğunu doğruladım.
openpgp-personal-no-label =
    .label = Hayır, kişisel anahtarım olarak kullanma.
openpgp-copy-cmd-label =
    .label = Kopyala

## e2e encryption settings

openpgp-add-key-button =
    .label = Anahtar ekle…
    .accesskey = e
e2e-learn-more = Daha fazla bilgi al
openpgp-keygen-success = OpenPGP anahtarı başarıyla oluşturuldu!
openpgp-keygen-import-success = OpenPGP anahtarları başarıyla içe aktarıldı!
openpgp-keygen-external-success = Harici GnuPG anahtar kimliği kaydedildi!

## OpenPGP Key selection area

openpgp-radio-none-desc = Bu kimlik için OpenPGP kullanma.
#   $key (String) - the expiration date of the OpenPGP key
openpgp-radio-key-expires = Son geçerlilik tarihi: { $date }
openpgp-key-expires-image =
    .tooltiptext = Anahtarın süresi 6 aydan kısa bir süre içinde dolacak
#   $key (String) - the expiration date of the OpenPGP key
openpgp-radio-key-expired = Son geçerlilik tarihi: { $date }
openpgp-key-expired-image =
    .tooltiptext = Anahtarın süresi doldu
openpgp-key-expand-section =
    .tooltiptext = Daha fazla bilgi
openpgp-key-revoke-title = Anahtarı iptal et
openpgp-key-edit-title = OpenPGP anahtarını değiştir
openpgp-key-edit-date-title = Geçerlilik tarihini uzat
openpgp-manager-button =
    .label = OpenPGP Anahtar Yöneticisi
    .accesskey = Y
openpgp-key-remove-external =
    .label = Harici anahtar kimliğini kaldır
    .accesskey = H
key-external-label = Harici GnuPG anahtarı
# Strings in keyDetailsDlg.xhtml
key-type-public = ortak anahtar
key-type-primary = birincil anahtar
key-type-subkey = alt anahtar
key-type-pair = anahtar çifti (gizli anahtar ve ortak anahtar)
key-expiry-never = hiçbir zaman
key-usage-encrypt = Şifrele
key-usage-sign = İmzala
key-expired-date = Anahtarın süresi { $keyExpiry } tarihinde doldu
key-expired-simple = Anahtarın süresi doldu
key-revoked-simple = Anahtar iptal edildi
key-do-you-accept = Dijital imzaları doğrulamak ve iletileri şifrelemek için bu anahtarı kabul ediyor musunuz?
# Strings enigmailMsgComposeOverlay.js
cannot-use-own-key-because = Kişisel anahtarınızla ilgili bir sorun olduğundan ileti gönderilemedi. { $problem }
cannot-encrypt-because-missing = Aşağıdaki alıcıların anahtarlarında sorun olduğu için bu ileti uçtan uca şifrelemeyle gönderilemedi: { $problem }
# Strings in keyserver.jsm
keyserver-error-aborted = Durduruldu
keyserver-error-unknown = Bilinmeyen bir hata oluştu
keyserver-error-server-error = Anahtar sunucusu bir hata bildirdi.
keyserver-error-import-error = İndirilen anahtar içe aktarılamadı.
keyserver-error-unavailable = Anahtar sunucusu kullanılamıyor.
keyserver-error-security-error = Anahtar sunucusu şifreli erişimi desteklemiyor.
keyserver-error-certificate-error = Anahtar sunucusunun sertifikası geçerli değil.
keyserver-error-unsupported = Anahtar sunucusu desteklenmiyor.
# Strings in gpg.jsm
unknown-signing-alg = Bilinmeyen imzalama algoritması (ID: { $id })
expiry-open-key-manager = OpenPGP anahtar yöneticisini aç
expiry-open-key-properties = Anahtar özelliklerini aç
# Strings filters.jsm
filter-folder-required = Bir hedef klasör seçmelisiniz.
filter-key-required = Bir alıcı anahtarı seçmelisiniz.
filter-encrypt-label = Anahtara şifrele (OpenPGP)
import-info-bits = Bit
import-info-fpr = Parmak izi
import-info-no-keys = İçe aktarılmış anahtar yok.
# Strings in enigmailKeyManager.js
import-from-clip = Panodan bazı anahtarları içe aktarmak istiyor musunuz?
copy-to-clipbrd-failed = Seçilen anahtar(lar) panoya kopyalanamadı.
copy-to-clipbrd-ok = Anahtar(lar) panoya kopyalandı
delete-selected-pub-key = Ortak anahtarları silmek istiyor musunuz?
key-man-loading-keys = Anahtarlar yükleniyor, lütfen bekleyin…
export-secret-key = Gizli anahtarı kaydedilmiş OpenPGP anahtar dosyasına eklemek istiyor musunuz?
save-keys-ok = Anahtarlar başarıyla kaydedildi
save-keys-failed = Anahtarların kaydedilmesi başarısız oldu
preview-failed = Ortak anahtar dosyası okunamıyor.
general-error = Hata: { $reason }
dlg-button-delete = &Sil

## Account settings export output

openpgp-export-public-fail = <b>Seçilen ortak anahtar dışa aktarılamadı.</b>
openpgp-export-secret-success = <b>Gizli anahtar başarıyla dışa aktarıldı.</b>
openpgp-export-secret-fail = <b>Seçilen gizli anahtar dışa aktarılamadı.</b>
# Strings in gnupg-keylist.jsm
keyring-photo = Fotoğraf
# Strings in key.jsm
already-revoked = Bu anahtar zaten iptal edilmiş.
openpgp-key-revoke-success = Anahtar başarıyla iptal edildi.
# Strings in keyRing.jsm & decryption.jsm
key-man-button-import = &İçe aktar
delete-key-title = OpenPGP anahtarını sil
delete-external-key-description = Bu harici GnuPG anahtar kimliğini kaldırmak istiyor musunuz?
key-in-use-title = Şu anda kullanılan OpenPGP anahtarı
key-error-not-accepted-as-personal = '{ $keySpec }' kimliğine sahip anahtarın kişisel anahtarınız olduğunu doğrulamadınız.
# Strings used in enigmailKeyManager.js & windows.jsm
need-online = Seçtiğiniz işlev çevrimdışı modda kullanılamaz. Lütfen çevrimiçi olun ve tekrar deneyin.
# Strings used in keyRing.jsm & keyLookupHelper.jsm
no-key-found = Belirtilen arama ölçütleriyle eşleşen bir anahtar bulamadık.
# Strings used in keyRing.jsm & GnuPGCryptoAPI.jsm
fail-key-extract = Hata - Anahtar çıkarma komutu başarısız oldu
# Strings used in keyRing.jsm
fail-cancel = Hata - Anahtar alımı kullanıcı tarafından iptal edildi
not-first-block = Hata - İlk OpenPGP bloku ortak anahtar bloku değil
fail-key-import = Hata - anahtar içe aktarma başarısız oldu
file-write-failed = { $output } dosyasına yazılamadı
no-pgp-block = Hata - Geçerli bir zırhlı OpenPGP veri bloku bulunamadı
# Strings used in trust.jsm
key-valid-unknown = bilinmiyor
key-valid-invalid = geçersiz
key-valid-disabled = devre dışı
key-valid-revoked = iptal edildi
key-valid-expired = süresi doldu
key-trust-untrusted = güvenilmeyen
key-trust-marginal = marjinal
key-trust-full = güvenilir
key-trust-group = (grup)
# Strings used in commonWorkflows.js
import-key-file = OpenPGP anahtar dosyasını içe aktar
import-rev-file = OpenPGP iptal dosyasını içe aktar
gnupg-file = GnuPG dosyaları
import-keys-failed = Anahtarlar içe aktarılamadı
passphrase-prompt = Lütfen şu anahtarın kilidini açan parolayı girin: { $key }
revoke-cert-failed = İptal sertifikası oluşturulamadı.
gen-going = Anahtar üretimi devam ediyor!
expiry-too-short = Anahtarınız en az bir gün geçerli olmalıdır.
expiry-too-long = Süresi 100 yıldan fazla olan bir anahtar oluşturamazsınız.
key-man-button-generate-key = Anahtar &oluştur
key-abort = Anahtar üretimi iptal edilsin mi?
# Strings used in enigmailMessengerOverlay.js
failed-decrypt = Hata - şifre çözme başarısız oldu
signature-verified-ok = { $attachment } ekinin imzası başarıyla doğrulandı
signature-verify-failed = { $attachment } ekinin imzası doğrulanamadı
msg-compose-details-button-label = Ayrıntılar…
msg-compose-details-button-access-key = n
send-aborted = Gönderme işlemi iptal edildi.
key-not-found = '{ $key }' anahtarı bulunamadı
key-revoked = '{ $key }' anahtarı iptal edildi
key-expired = '{ $key }' anahtarının süresi doldu
msg-compose-internal-error = Dahili bir hata oluştu.
msg-compose-cannot-save-draft = Taslak kaydedilirken hata oluştu
save-attachment-header = Şifresi çözülmüş eki kaydet
no-temp-dir =
    Yazılacak geçici klasör bulunamadı
    Lütfen TEMP ortam değişkenini ayarlayın
# Strings used in decryption.jsm
do-import-multiple =
    Aşağıdaki anahtarlar içe aktarılsın mı?
    { $key }
do-import-one = { $name } ({ $id }) içe aktarılsın mı?
unverified-reply = Girintili ileti kısmı (yanıt) muhtemelen değiştirilmiş
sig-mismatch = Hata - İmza uyuşmazlığı
invalid-email = Hata: geçersiz e-posta adres(ler)i
dlg-button-view = &Göster
# Strings used in encryption.jsm
not-required = Hata - şifreleme gerekmiyor
# Strings used in windows.jsm
no-photo-available = Fotoğraf yok
error-photo-path-not-readable = '{ $photo }' fotoğraf yolu okunamıyor
debug-log-title = OpenPGP hata ayıklama günlüğü
# Strings used in dialog.jsm
repeat-prefix = Bu uyarı { $count }
repeat-suffix-singular = kere daha tekrarlanacak.
repeat-suffix-plural = kere daha tekrarlanacak.
no-repeat = Bu uyarı bir daha gösterilmeyecek.
dlg-keep-setting = Yanıtımı hatırla ve bir daha sorma
dlg-button-ok = &Tamam
dlg-button-close = &Kapat
dlg-button-cancel = &Vazgeç
dlg-no-prompt = Bu iletişim kutusunu bir daha gösterme
enig-prompt = OpenPGP İstemi
enig-confirm = OpenPGP Onayı
enig-alert = OpenPGP Uyarısı
enig-info = OpenPGP Bilgilendirmesi
# Strings used in persistentCrypto.jsm
dlg-button-retry = &Yeniden dene
dlg-button-skip = &Geç
# Strings used in enigmailCommon.js
enig-error = OpenPGP hatası
enig-alert-title =
    .title = OpenPGP uyarısı
