# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

refresh-profile-dialog =
    .title = { -brand-short-name } – შეკეთება
refresh-profile-dialog-button =
    .label = { -brand-short-name } – შეკეთება
refresh-profile-description = გაუშვით Firefox პირვანდელი მდგომარეობით, გაუმართაობების გასწორებისა და წარმადობის აღსადგენად.
refresh-profile-description-details = შედეგად:
refresh-profile-remove = მოცილდება დამატებები და მორგებული პარამეტრები
refresh-profile-restore = ბრაუზერი ნაგულისხმევ პარამეტრებზე დაბრუნდება
refresh-profile = მოაწესრიგეთ { -brand-short-name }
refresh-profile-button = { -brand-short-name } – შეკეთება…
