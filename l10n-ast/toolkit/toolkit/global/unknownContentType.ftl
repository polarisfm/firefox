# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

unknowncontenttype-settingschange =
    .value =
        { PLATFORM() ->
            [windows] Los axustes puen camudase nes opciones de { -brand-short-name }.
           *[other] Los axustes puen camudase nes preferencies de { -brand-short-name }.
        }
