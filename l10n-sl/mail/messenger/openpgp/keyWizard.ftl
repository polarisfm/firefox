# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#   $identity (String) - the email address of the currently selected identity
key-wizard-dialog-window =
    .title = Dodaj osebni ključ OpenPGP za  { $identity }
key-wizard-button =
    .buttonlabelaccept = Nadaljuj
    .buttonlabelhelp = Pojdi nazaj

## Generate key section


## Import Key section


## External Key section

