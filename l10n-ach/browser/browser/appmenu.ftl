# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-update-banner =
    .label-update-downloading = Gamo ngec manyen pi { -brand-shorter-name }
appmenuitem-customize-mode =
    .label = Yiki…

## Zoom Controls

appmenuitem-zoom-enlarge =
    .label = Kwot madit
appmenuitem-zoom-reduce =
    .label = Jwik matidi

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Rib Kombedi

## What's New panel in App menu.

whatsnew-panel-header = Ngo Manyen
# Checkbox displayed at the bottom of the What's New panel, allowing users to
# enable/disable What's New notifications.
whatsnew-panel-footer-checkbox =
    .label = Mi ngec pi jami manyen
    .accesskey = m
