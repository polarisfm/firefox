# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

openpgp-view-signer-key =
    .label = İmzalayan anahtarını görüntüle
openpgp-view-your-encryption-key =
    .label = Şifre çözme anahtarımı görüntüle
openpgp-openpgp = OpenPGP
openpgp-no-sig = Dijital imza yok
openpgp-uncertain-sig = Belirsiz dijital imza
openpgp-invalid-sig = Geçersiz dijital imza
openpgp-good-sig = İyi dijital imza
openpgp-sig-key-id = İmzalayan anahtar kimliği: { $key }
openpgp-sig-key-id-with-subkey-id = İmzalayan anahtar kimliği: { $key } (Alt anahtar kimliği: { $subkey })
openpgp-enc-key-id = Şifre çözme anahtarı kimliğiniz: { $key }
openpgp-enc-key-with-subkey-id = Şifre çözme anahtarı kimliğiniz: { $key } (Alt anahtar kimliği: { $subkey })
openpgp-unknown-key-id = Bilinmeyen anahtar
openpgp-other-enc-additional-key-ids = Ayrıca, ileti aşağıdaki anahtarların sahiplerine şifrelenmiştir:
openpgp-other-enc-all-key-ids = İleti, aşağıdaki anahtarların sahiplerine şifrelenmiştir:
