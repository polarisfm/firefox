# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxa-toolbar-sync-syncing =
    .label = Synchronizuje sa…
fxa-toolbar-sync-syncing-tabs =
    .label = Synchronizujem karty…
sync-disconnect-dialog-title = Odpojiť zo služby { -sync-brand-short-name }?
sync-disconnect-dialog-body = { -brand-product-name } ukončí synchronizáciu s vašim účtom ale neodstráni z tohto zariadenia žiadne údaje.
fxa-disconnect-dialog-title = Odpojiť { -brand-product-name }?
fxa-disconnect-dialog-body = { -brand-product-name } sa odpojí od vášho účtu ale neodstráni z tohto zariadenia žiadne údaje.
sync-disconnect-dialog-button = Odpojiť
fxa-signout-dialog-heading = Odhlásiť sa z { -fxaccount-brand-name(case: "gen", capitalization: "lower") }?
fxa-signout-dialog-body = Synchronizované údaje zostanú vo vašom účte.
fxa-signout-checkbox =
    .label = Odstrániť údaje z tohto zariadenia (prihlasovacie údaje, heslá, históriu, záložky atď.)
fxa-signout-dialog =
    .title = Odhlásenie sa z { -fxaccount-brand-name(case: "gen", capitalization: "lower") }
    .style = min-width: 375px;
    .buttonlabelaccept = Odhlásiť sa
