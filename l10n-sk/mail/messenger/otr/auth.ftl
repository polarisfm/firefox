# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

auth-yes =
    .label = Áno
auth-no =
    .label = Nie
auth-manualVerification-label =
    .label = { auth-manualVerification }
auth-questionAndAnswer-label =
    .label = { auth-questionAndAnswer }
auth-sharedSecret-label =
    .label = { auth-sharedSecret }
auth-question = Zadajte otázku:
auth-answer = Zadajte odpoveď (rozlišujú sa veľké a malé písmená):
