# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

openpgp-key-man-file-menu =
    .label = Файл
    .accesskey = Ф
openpgp-key-man-edit-menu =
    .label = Түзету
    .accesskey = е
openpgp-key-man-view-menu =
    .label = Түрі
    .accesskey = Т
openpgp-key-man-close =
    .label = Жабу
openpgp-copy-cmd-label =
    .label = Көшіріп алу

## e2e encryption settings


## OpenPGP Key selection area


## Account settings export output

