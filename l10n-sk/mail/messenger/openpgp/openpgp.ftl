# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

openpgp-key-gen-key-type-rsa =
    .label = RSA
openpgp-key-man-close =
    .label = Zavrieť
openpgp-key-man-key-more =
    .label = Viac
    .accesskey = V
openpgp-key-man-fingerprint-label =
    .label = Odtlačok prsta

## e2e encryption settings

e2e-learn-more = Ďalšie informácie

## OpenPGP Key selection area


## Account settings export output

