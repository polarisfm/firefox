# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
---
job-defaults:
    max-run-time: 1800
    suite: raptor
    run-on-projects:
        by-test-platform:
            android-hw-p2-.*-api-16/(?!opt).*: []
            android-hw-p2-.*-api-16-shippable/opt: []
            android-hw(?!-p2).*(?<!-api-16-shippable)/opt.*: []
            default: ['mozilla-central']
    target: geckoview_example.apk
    test-manifest-loader: null  # don't load tests in the taskgraph
    tier: 1
    virtualization: hardware
    mozharness:
        script: raptor_script.py
        config:
            - raptor/android_hw_config.py
        extra-options:
            - --no-conditioned-profile
    optimization:
        skip-unless-backstop: null

raptor-speedometer-geckoview:
    description: "Raptor Speedometer on GeckoView"
    try-name: raptor-speedometer-geckoview
    treeherder-symbol: Rap(sp)
    run-on-projects:
        by-test-platform:
            android-hw-.*(?<!-shippable)/opt: []
            android-hw-p2-.*api-16/pgo: []
            android-hw-p2-.*aarch64.*/pgo: ['trunk', 'mozilla-beta']
            android-hw-p2-.*aarch64-shippable/opt: ['trunk', 'mozilla-beta']
            android-hw-g5.*/pgo: ['trunk', 'mozilla-beta']
            android-hw-g5.*-shippable/opt: ['trunk', 'mozilla-beta']
            default: ['mozilla-central', 'mozilla-beta']
    max-run-time: 900
    mozharness:
        extra-options:
            - --test=raptor-speedometer
            - --app=geckoview
            - --binary=org.mozilla.geckoview_example
            - --activity=org.mozilla.geckoview_example.GeckoViewActivity

raptor-speedometer-geckoview-cpu-memory:
    description: "Raptor Speedometer cpu/memory on GeckoView"
    try-name: raptor-speedometer-geckoview-cpu-memory
    treeherder-symbol: Rap(sp-cm)
    target: geckoview_example.apk
    run-on-projects: []
    max-run-time: 1800
    mozharness:
        extra-options:
            - --test=raptor-speedometer
            - --app=geckoview
            - --binary=org.mozilla.geckoview_example
            - --cpu-test
            - --memory-test
            - --page-cycles 5
            - --activity=org.mozilla.geckoview_example.GeckoViewActivity

raptor-speedometer-geckoview-cpu-memory-power:
    description: "Raptor Speedometer cpu/memory/power on GeckoView"
    try-name: raptor-speedometer-geckoview-cpu-memory-power
    treeherder-symbol: Rap(sp-cmp)
    target: geckoview_example.apk
    run-on-projects: []
    max-run-time: 1800
    mozharness:
        extra-options:
            - --test=raptor-speedometer
            - --app=geckoview
            - --binary=org.mozilla.geckoview_example
            - --cpu-test
            - --memory-test
            - --power-test
            - --page-cycles 5
            - --activity=org.mozilla.geckoview_example.GeckoViewActivity

raptor-scn-cpu-memory-idle-geckoview:
    description: "Raptor idle-browser cpu/memory on GeckoView"
    try-name: raptor-scn-cpu-memory-idle-geckoview
    treeherder-symbol: Rap(idl-cm)
    run-on-projects: []
    mozharness:
        extra-options:
            - --test=raptor-scn-power-idle
            - --app=geckoview
            - --binary=org.mozilla.geckoview_example
            - --cpu-test
            - --memory-test
            - --activity=org.mozilla.geckoview_example.GeckoViewActivity

raptor-scn-cpu-memory-power-idle-geckoview:
    description: "Raptor idle-browser cpu/memory/power on GeckoView"
    try-name: raptor-scn-cpu-memory-power-idle-geckoview
    treeherder-symbol: Rap(idl-cmp)
    run-on-projects: []
    mozharness:
        extra-options:
            - --test=raptor-scn-power-idle
            - --app=geckoview
            - --binary=org.mozilla.geckoview_example
            - --cpu-test
            - --memory-test
            - --power-test
            - --activity=org.mozilla.geckoview_example.GeckoViewActivity

raptor-scn-cpu-memory-idle-bg-geckoview:
    description: "Raptor idle-browser (backgrounded) cpu/memory on GeckoView"
    try-name: raptor-scn-cpu-memory-idle-bg-geckoview
    treeherder-symbol: Rap(idlbg-cm)
    run-on-projects: []
    mozharness:
        extra-options:
            - --test=raptor-scn-power-idle-bg
            - --app=geckoview
            - --binary=org.mozilla.geckoview_example
            - --cpu-test
            - --memory-test
            - --activity=org.mozilla.geckoview_example.GeckoViewActivity

raptor-scn-cpu-memory-power-idle-bg-geckoview:
    description: "Raptor idle-browser (backgrounded) cpu/memory/power on GeckoView"
    try-name: raptor-scn-cpu-memory-power-idle-bg-geckoview
    treeherder-symbol: Rap(idlbg-cmp)
    run-on-projects: []
    mozharness:
        extra-options:
            - --test=raptor-scn-power-idle-bg
            - --app=geckoview
            - --binary=org.mozilla.geckoview_example
            - --cpu-test
            - --memory-test
            - --power-test
            - --activity=org.mozilla.geckoview_example.GeckoViewActivity

raptor-unity-webgl-geckoview:
    description: "Raptor Unity WebGL on GeckoView"
    try-name: raptor-unity-webgl-geckoview
    treeherder-symbol: Rap(ugl)
    run-on-projects:
        by-test-platform:
            android-hw-.*(?<!-shippable)/opt: []
            android-hw-p2-.*api-16/pgo: []
            android-hw-p2-.*api-16-shippable/opt: []
            android-hw-(?!p2-.*api-16)/pgo: ['trunk', 'mozilla-beta']
            android-hw-(?!p2-.*api-16)-shippable/opt: ['trunk', 'mozilla-beta']
            default: ['mozilla-central', 'mozilla-beta']
    max-run-time: 900
    mozharness:
        extra-options:
            - --test=raptor-unity-webgl
            - --app=geckoview
            - --binary=org.mozilla.geckoview_example
            - --activity=org.mozilla.geckoview_example.GeckoViewActivity
    fetches:
        fetch:
            - unity-webgl
