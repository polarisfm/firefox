# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at http://mozilla.org/MPL/2.0/.

places-open =
    .label = Ашу
    .accesskey = А
places-open-tab =
    .label = Жаңа бетте ашу
    .accesskey = т
places-open-all-in-tabs =
    .label = Әрқайсысын жаңа бетте ашу
    .accesskey = с
places-open-window =
    .label = Жаңа терезеде ашу
    .accesskey = т
places-open-private-window =
    .label = Жаңа жекелік терезесінде ашу
    .accesskey = к
places-new-bookmark =
    .label = Жаңа бетбелгі…
    .accesskey = б
places-new-folder-contextmenu =
    .label = Жаңа бума…
    .accesskey = м
places-new-folder =
    .label = Жаңа бума…
    .accesskey = у
places-new-separator =
    .label = Жаңа ажыратқыш
    .accesskey = а
places-view =
    .label = Қарау
    .accesskey = р
places-by-date =
    .label = Күні бойынша
    .accesskey = н
places-by-site =
    .label = Сайт бойынша
    .accesskey = й
places-by-most-visited =
    .label = Көбірек қаралғаны бойынша
    .accesskey = е
places-by-last-visited =
    .label = Соңғы қаралғаны бойынша
    .accesskey = о
places-by-day-and-site =
    .label = Күн мен сайт бойынша
    .accesskey = м
places-history-search =
    .placeholder = Тарихтан іздеу
places-bookmarks-search =
    .placeholder = Бетбелгілерден іздеу
places-delete-domain-data =
    .label = Бұл парақты өшіру
    .accesskey = Б
places-sortby-name =
    .label = Аты бойынша сұрыптау
    .accesskey = й
places-properties =
    .label = Қасиеттері
    .accesskey = е
# Managed bookmarks are created by an administrator and cannot be changed by the user.
managed-bookmarks =
    .label = Басқарылатын бетбелгілер
# This label is used when a managed bookmarks folder doesn't have a name.
managed-bookmarks-subfolder =
    .label = Ішкі бума
