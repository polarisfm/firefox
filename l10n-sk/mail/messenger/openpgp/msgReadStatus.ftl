# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

openpgp-openpgp = OpenPGP
openpgp-no-sig = Žiadny digitálny podpis
openpgp-uncertain-sig = Pochybný digitálny podpis
openpgp-invalid-sig = Neplatný digitálny podpis
openpgp-good-sig = Dobrý digitálny podpis
openpgp-unknown-key-id = Neznámy kľúč
