# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The question portion of the following message should have the <strong> and </strong> tags surrounding it.
default-browser-notification-message = <strong>Itakda ang { -brand-short-name } bilang iyong default browser?</strong> Kumuha ng mabilis, ligtas, at pribadong pagba-browse tuwing gagamit ka ng web.
default-browser-notification-button =
    .label = Itakda bilang Default
    .accesskey = S
