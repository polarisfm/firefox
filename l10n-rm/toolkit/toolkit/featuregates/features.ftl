# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-masonry2 =
    .label = CSS: Masonry Layout
experimental-features-css-masonry-description = Activescha il sustegn per per la funcziunalitad experimentala CSS Masonry Layout. Consultar <a data-l10n-name="explainer">l'explicaziun</a> per infurmaziuns detagliadas davart la funcziun. Per dar in resun, commentescha per plaschair en <a data-l10n-name="w3c-issue">quest tema da GitHub</a> u <a data-l10n-name="bug">quest bug</a>.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-gpu2 =
    .label = Web API: WebGPU
experimental-features-web-gpu-description2 = Questa nova API porscha in sustegn low-level per calculaziuns e la visualisaziun grafica cun agid da <a data-l10n-name="wikipedia">GPU</a> da l'apparat u dal computer da l'utilisader. La <a data-l10n-name="spec">specificaziun</a> è anc adina en lavur. Vesair <a data-l10n-name="bugzilla">bug 1602129</a> per ulteriurs detagls.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-media-avif =
    .label = Media: AVIF
experimental-features-media-avif-description = Sche questa funcziunalitad è activada, sustegna { -brand-short-name } il format da datotecas da maletg AV1 (AVIF). Quai è in format da datotecas per maletgs fixs che sa serva dals algoritmus da cumpressiun per video AV1 per reducir la grondezza dal maletg. Vesair <a data-l10n-name="bugzilla">bug 1443863</a> per ulteriurs detagls.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-inputmode =
    .label = Web API: inputmode
# "inputmode" and "contenteditable" are technical terms and shouldn't be translated.
experimental-features-web-api-inputmode-description = Nossa implementaziun da l'attribut global <a data-l10n-name="mdn-inputmode">inputmode</a> è vegnida actualisada tenor la <a data-l10n-name="whatwg">specificaziun WHATWG</a> ma i dovra anc ulteriuras midadas, sco al render disponibel per cuntegns «contenteditable». Vesair <a data-l10n-name="bugzilla">bug </a></a> per ulteriurs detagls.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-link-preload =
    .label = Web API: <link rel="preload">
# Do not translate "rel", "preload" or "link" here, as they are all HTML spec
# values that do not get translated.
experimental-features-web-api-link-preload-description = L'attribut <a data-l10n-name="rel">rel</a> cun la valur <code>"preload"</code> en in element <a data-l10n-name="link">&lt;link&gt;</a> ha l'intenziun da meglierar la prestaziun cun permetter da telechargiar resursas gia pli baud en il ciclus vital dad ina pagina. Quai garantescha che las resursas stattan a disposiziun pli baud e la probabilitad ch'ellas blocheschan la visualisaziun da la pagina sa diminuescha. Ulteriuras infurmaziuns sut <a data-l10n-name="readmore">«Prechargiar cuntegn cun <code>rel="preload"</code>»</a> u <a data-l10n-name="bugzilla">bug 1583604</a>.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-focus-visible =
    .label = CSS: Pseudo-class: :focus-visible
experimental-features-css-focus-visible-description = Permetta dad applitgar stils da focus ad elements sco buttuns e controllas da formulars sch'els vegnan focussads cun agid da la tastatura (p.ex. cun siglir cun la tasta da tabulatur da in element a l'auter), ma betg sch'els vegnan focussads cun la mieur u auters apparats d'indicaziun. Vesair <a data-l10n-name="bugzilla">bug 1617600</a> per ulteriurs detagls.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-beforeinput =
    .label = Web API: eveniment beforeinput
# The terms "beforeinput", "input", "textarea", and "contenteditable" are technical terms
# and shouldn't be translated.
experimental-features-web-api-beforeinput-description = L'eveniment global <a data-l10n-name="mdn-beforeinput">beforeinput</a> dat in impuls sin elements <a data-l10n-name="mdn-input">&lt;input&gt;</a> e <a data-l10n-name="mdn-textarea">&lt;textarea&gt;</a> u mintga element dal qual l'attribut <a data-l10n-name="mdn-contenteditable">contenteditable</a> è activà, immediatamain avant che la valur da l'element sa mida. L'eveniment permetta ad applicaziuns da web da midar il cumportament predefinì dal navigatur per interacziuns cun l'utilisader. Applicaziuns da web pon impedir l'endataziun per tscherts caracters specifics u pon modifitgar l'encollar da text cun stil mo per stils approvads.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-constructable-stylesheets =
    .label = CSS: Constructable Stylesheets
experimental-features-css-constructable-stylesheets-description = L'agiunta dad in constructur a l'interfatscha <a data-l10n-name="mdn-cssstylesheet">CSSStyleSheet</a> ed ina retscha da midadas en quest connex pussibiliteschan da crear directamain novs stylesheets senza stuair agiuntar il sheet al HTML. Quai renda bler pli simpel da crear stylesheets reutilisabels per l'utilisaziun cun <a data-l10n-name="mdn-shadowdom">Shadow DOM</a>. Vesair <a data-l10n-name="bugzilla">bug 1520690</a> per ulteriurs detagls.
experimental-features-devtools-color-scheme-simulation =
    .label = Utensils per sviluppaders: simulaziun da schemas da colurs
experimental-features-devtools-color-scheme-simulation-description = Agiuntescha ina opziun per simular differents schemas da colurs che ta permetta da testar dumondas da medias <a data-l10n-name="mdn-preferscolorscheme">@prefers-color-scheme</a>. L'utilisaziun da questa dumonda da medias permetta a tes stylesheet da respunder schebain l'utilisader preferescha ina interfatscha clera u stgira. Cun questa funcziunalitad pos ti testar tes code senza stuair midar parameters en tes navigatur (u en il sistem operativ sche tes navigatur s'adatta ad in parameter per il schema da colurs sin nivel dal sistem operativ). Vesair <a data-l10n-name="bugzilla1">bug 1550804</a> e <a data-l10n-name="bugzilla2">bug 1137699</a> per ulteriurs detagls.
experimental-features-devtools-execution-context-selector =
    .label = Utensils per sviluppaders: selecziun dal context d'execuziun
experimental-features-devtools-execution-context-selector-description = Questa funcziunalitad mussa in buttun en la lingia da cumonds da la consola che ta permetta da midar il context en il qual l'expressiun che ti endateschas vegn exequida. Vesair <a data-l10n-name="bugzilla1">bug 1605154</a> e <a data-l10n-name="bugzilla2">bug 1605153</a> per ulteriurs detagls.
experimental-features-devtools-compatibility-panel =
    .label = Utensils per sviluppaders: Panela da cumpatibilitad
experimental-features-devtools-compatibility-panel-description = Ina panela laterala per l'inspectur da pagina che ta mussa infurmaziuns detagliadas davart il status da cumpatibilitad da tia app cun differents navigaturs. Vesair <a data-l10n-name="bugzilla">bug 1584464</a> per ulteriurs detagls.
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-lax-by-default2 =
    .label = Cookies: SameSite=Lax tenor standard
experimental-features-cookie-samesite-lax-by-default2-description = Tracta cookies tenor standard sco «SameSite=Lax» sche nagin attribut «SameSite» è specifitgà. Sviluppaders ston inditgar explicit «SameSite=None» per redefinir il funcziunament actual che na prevesa nagina limitaziun d'utilisaziun.
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-none-requires-secure2 =
    .label = Cookies: SameSite=None pretenda l'attribut «secure»
experimental-features-cookie-samesite-none-requires-secure2-description = Cookies cun l'attribut «SameSite=None» pretendan l'attribut «secure». Premissa per questa funcziunalitad è «Cookies: SameSite=Lax tenor standard».
# about:home should be kept in English, as it refers to the the URI for
# the internal default home page.
experimental-features-abouthome-startup-cache =
    .label = Cache inizial per about:home
experimental-features-abouthome-startup-cache-description = In cache per il document inizial about:home che vegn chargià tenor standard cun aviar. L'intent dal cache è da meglierar la performanza cun aviar.
experimental-features-print-preview-tab-modal =
    .label = Prevista da stampa redissegnada
experimental-features-print-preview-tab-modal-description = Activescha la prevista da stampa redissegnada e renda disponibla la prevista da stampa sin macOS. La funcziunalitad po chaschunar errurs e n'includa anc betg tut ils parameters en connex cun la stampa. Per acceder a tut ils parameters da stampa disponibels, tscherner «Stampar cun agid dal dialog dal sistem…”» en la panela da stampa.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-cookie-samesite-schemeful =
    .label = Cookies: Schemeful SameSite
experimental-features-cookie-samesite-schemeful-description = Tracta cookies da la medema domena, ma cun differents schemas (p.ex. http://example.com e https://example.com) sco cookies da differentas websites (e betg sco cookies da la medema website). Quai augmenta la segirezza, ma po chaschunar problems da navigaziun.
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support =
    .label = Utensils per sviluppaders: Debugadi da service worker
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support-description = Activescha il sustegn experimental per service worker en la panela da debugadi. Questa funcziunalitad po ralentar ils utensils per sviluppaders ed augmentar il consum da memoria.
# Desktop zooming experiment
experimental-features-graphics-desktop-zooming =
    .label = Grafica: Zoom fluid cun avischinar la detta (Smooth Pinch Zoom)
experimental-features-graphics-desktop-zooming-description = Activescha il sustegn da zoom fluid cun avischinar la detta sin moniturs tactils e touch pads da precisiun.
# WebRTC global mute toggle controls
experimental-features-webrtc-global-mute-toggles =
    .label = Activaziun/deactivaziun globala dad audio e video WebRTC
experimental-features-webrtc-global-mute-toggles-description = Agiunta controllas a l'indicatur da cundivisiun WebRTC global che permettan als utilisaders da deactivar globalmain il tun u stream da lur microfon u camera.
# JS JIT Warp project
experimental-features-js-warp =
    .label = JavaScript JIT: Warp
experimental-features-js-warp-description = Activar Warp, in project per meglierar la performanza ed il consum da memoria da JavaScript.
# Fission is the name of the feature and should not be translated.
experimental-features-fission =
    .label = Fission (isolaziun da websites)
experimental-features-fission-description = Fission (isolaziun da websites) è ina funcziunalitad experimentala da { -brand-short-name } per porscher in nivel da defensiun supplementar cunter mancos da segirezza. Cun isolar mintga website en in process separà, renda Fission pli difficil a websites maliziusas dad acceder ad infurmaziuns dad autras paginas che ti visitas. Quai è ina midada architectonica pli gronda en { -brand-short-name } e nus appreziain sche ti testeschas e rapportas eventuals problems chattads. Per ulteriurs detagls, vesair <a data-l10n-name="wiki">il wiki</a>.
