# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Fission is the name of the feature and should not be translated.
experimental-features-fission =
    .label = الانشطار (عزل المواقع)
experimental-features-fission-description = ميزة ”الانشطار/Fission“ (عزل المواقع) هي ميزة تجريبية في { -brand-short-name } تقدّم طبقة حماية دفاعية إضافية ضد العلل الأمنية. إذ تصعّب ميزة ”الانشطار“ على المواقع الخبيثة من الوصول إلى معلوماتك في بقية الصفحات التي تزور وذلك عبر عزل كل موقع عن غيره في سيرورة منفصلة. يُعدّ هذا تغييرا مفصليًا في بنية { -brand-short-name } ونقدّر كل الدعم منك في اختبارها والإبلاغ عن أية مشاكل تواجهها. طالِع <a data-l10n-name="wiki">الويكي</a> لتفاصيل أكثر.
