# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-httpsonly-insecure-title = A connexión segura no ye disponible
about-httpsonly-button-make-exception = Acceptar lo risgo y continar enta lo puesto
about-httpsonly-title = { -brand-short-name } ha detectau un problema potencial de seguranza
about-httpsonly-explanation-question = Qué puede estar causando esto?
about-httpsonly-button-accept-and-continue = Acceptar lo risgo y continar
about-httpsonly-button-go-back = Ir enta zaga
about-httpsonly-link-learn-more = Saber-ne mas…
