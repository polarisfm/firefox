# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

openpgp-manage-keys-openpgp-cmd =
    .label = OpenPGP Anahtar Yöneticisi
    .accesskey = O
openpgp-ctx-decrypt-open =
    .label = Şifreyi çöz ve aç
    .accesskey = Ç
openpgp-ctx-decrypt-save =
    .label = Şifreyi çöz ve farklı kaydet…
    .accesskey = f
openpgp-ctx-import-key =
    .label = OpenPGP anahtarını içe aktar
    .accesskey = i
openpgp-ctx-verify-att =
    .label = İmzayı doğrula
    .accesskey = d
openpgp-has-sender-key = Bu ileti, gönderenin OpenPGP ortak anahtarını içerdiğini belirtiyor.
openpgp-be-careful-new-key = Uyarı: Bu iletideki yeni ortak genel anahtarı, daha önce { $email } için kabul ettiğiniz ortak anahtarlardan farklı.
openpgp-import-sender-key =
    .label = İçe aktar…
openpgp-search-keys-openpgp =
    .label = OpenPGP anahtarını keşfet
openpgp-missing-signature-key = Bu ileti henüz sahip olmadığınız bir anahtarla imzalandı.
openpgp-search-signature-key =
    .label = Keşfet…
openpgp-broken-exchange-repair =
    .label = İletiyi onar
openpgp-broken-exchange-wait = Lütfen bekleyin…
openpgp-cannot-decrypt-because-missing-key = Bu iletinin şifresini çözmek için gereken gizli anahtar mevcut değil.
openpgp-reminder-partial-display = Hatırlatma: Aşağıda gösterilen ileti, asıl iletinin yalnızca bir alt kümesidir.
openpgp-partial-verify-button = Doğrula
openpgp-partial-decrypt-button = Şifreyi çöz
