# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-update-banner =
    .label-update-downloading = { -brand-shorter-name } yeniləməsi endirilir
appmenuitem-customize-mode =
    .label = Fərdiləşdir…

## Zoom Controls

appmenuitem-zoom-enlarge =
    .label = Yaxınlaşdır
appmenuitem-zoom-reduce =
    .label = Uzaqlaşdır

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = İndi Sinxronizə et

## What's New panel in App menu.

