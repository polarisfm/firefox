# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-reload-selected-tab =
    .label = Обновить выделенную вкладку
    .accesskey = к
toolbar-context-menu-reload-selected-tabs =
    .label = Обновить выделенные вкладки
    .accesskey = к
toolbar-context-menu-bookmark-selected-tab =
    .label = Добавить в закладки выбранную вкладку…
    .accesskey = ы
toolbar-context-menu-bookmark-selected-tabs =
    .label = Добавить в закладки выбранные вкладки…
    .accesskey = е
toolbar-context-menu-select-all-tabs =
    .label = Выбрать все вкладки
    .accesskey = л
toolbar-context-menu-undo-close-tabs =
    .label =
        { $tabCount ->
            [1] Восстановить закрытую вкладку
            [one] Восстановить закрытую вкладку
            [few] Восстановить закрытые вкладки
           *[many] Восстановить закрытые вкладки
        }
    .accesskey = н
toolbar-context-menu-manage-extension =
    .label = Управление расширением
    .accesskey = в
toolbar-context-menu-remove-extension =
    .label = Удалить расширение
    .accesskey = л
# This label is used in the extensions toolbar buttons context menus,
# a user can use this command to submit to Mozilla an abuse report
# related to that extension. "Report" is a verb.
toolbar-context-menu-report-extension =
    .label = Пожаловаться на расширение
    .accesskey = ж
# Can appear on the same context menu as menubarCmd ("Menu Bar") and
# personalbarCmd ("Bookmarks Toolbar"), so they should have different
# access keys.
toolbar-context-menu-pin-to-overflow-menu =
    .label = Прикрепить в расширенном меню
    .accesskey = к
toolbar-context-menu-auto-hide-downloads-button =
    .label = Автоматически скрывать на панели инструментов
    .accesskey = о
toolbar-context-menu-remove-from-toolbar =
    .label = Убрать с панели инструментов
    .accesskey = и
toolbar-context-menu-view-customize-toolbar =
    .label = Персонализация…
    .accesskey = о
toolbar-context-menu-bookmarks-toolbar-always-show =
    .label = Всегда
    .accesskey = е
toolbar-context-menu-bookmarks-toolbar-never-show =
    .label = Никогда
    .accesskey = и
toolbar-context-menu-bookmarks-toolbar-on-new-tab =
    .label = Только в новой вкладке
    .accesskey = н
