# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-masonry2 =
    .label = CSS : Masonry Layout
experimental-features-css-masonry-description = Activa la presa en caarga de la foncionalitat experimentala CSS Masonry Layout. Consultatz <a data-l10n-name="explainer">aquesta explicacion</a> per una descripcion de naut nivèl de la foncionalitat. Per enviar vòstres comentaris, mercés de comentar dins <a data-l10n-name="w3c-issue">aqueste senhalament de bug</a> o <a data-l10n-name="bug">aqueste bug</a>.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-gpu2 =
    .label = Web API : WebGPU
experimental-features-web-gpu-description2 = Aquesta novèla API fornís una presa en carga de nivèl bas per efectuar de calculs e de rendiments grafics amb lo <a data-l10n-name="wikipedia">processor grafic</a> del periferic o de l’ordenador de l’utilizaire. L’<a data-l10n-name="spec">especificacion</a> es encara en òbra. Vejatz lo <a data-l10n-name="bugzilla">bug 1602129</a> per mai de detalhs.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-media-avif =
    .label = Mèdia : AVIF
experimental-features-media-avif-description = Quand aquesta foncionalitat es activada, { -brand-short-name } prend en carga lo format AV1 Image File (AVIF). Aqueste format de fichièr d’imatge fixe utiliza las capacitats dels algoritmes de compression vidèo AV1 per reduire la talha de l’imatge. Consultatz aqueste <a data-l10n-name="bugzilla">bug 1443863</a> per mai de detalhs.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-inputmode =
    .label = Web API : inputmode
# "inputmode" and "contenteditable" are technical terms and shouldn't be translated.
experimental-features-web-api-inputmode-description = Nòstra implementacion de l’atribut global <a data-l10n-name="mdn-inputmode">inputmode</a> foguèt mesa a jorn respècte a <a data-l10n-name="whatwg">la especificacion WHATWG</a>, mas nos cal encara aportar d’unas modificacions, tal coma la va venir disponibla per de contengut modificables via contenteditable. Vejatz lo <a data-l10n-name="bugzilla">bug 1205133</a> per mai de detalhs.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-link-preload =
    .label = Web API : <link rel="preload">
# Do not translate "rel", "preload" or "link" here, as they are all HTML spec
# values that do not get translated.
experimental-features-web-api-link-preload-description = L’atribut <a data-l10n-name="rel">rel</a> amb la valor « <code>"preload"</code> » sus un element <a data-l10n-name="link">&lt;link&gt;</a> a per tòca de melhorar las performanças en telecargant en avança las ressorsas de las paginas, en vos assegurant que son disponiblas e que blocaràn pas l’afichatge de la pagina. Legissètz <a data-l10n-name="readmore">« Precargament de contengut amb <code>rel="preload"</code> »</a> o vejatz aqueste <a data-l10n-name="bugzilla">bug 1583604</a> per mai de detalhs.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-focus-visible =
    .label = CSS : Pseudo-class: :focus-visible
experimental-features-css-focus-visible-description = Permet d’aplicar d’estils de focus a d’elements tal coma de botons e de contraròtles, sonque quand son seleccionats al clavièr (per exemple amb una tabulacion entre los elements) e non pas amb la mirga o un autre dispositiu de puntatge. Vejatz lo <a data-l10n-name="bugzilla">bug 1617600</a> per mai de detalhs.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-beforeinput =
    .label = Web API : beforeinput Event
# The terms "beforeinput", "input", "textarea", and "contenteditable" are technical terms
# and shouldn't be translated.
experimental-features-web-api-beforeinput-description = L’eveniment global <a data-l10n-name="mdn-beforeinput">beforeinput<a> es activat suls elements <a data-l10n-name="mdn-input">&lt;input&gt;</a> e <a data-l10n-name="mdn-textarea">&lt;textarea&gt;</a>, o quin element que siá que l’atribut <a data-l10n-name="mdn-contenteditable">contenteditable</a> es definit, sul pic abans que l’element càmbie de valor. Per exemple, las aplicacions web pòdon anullar çò que pica l’utilizaire unicament per de caractèrs especifics, o pòdon modificar l’empegatge d’un tèxt format unicament amb d’estils aprovats.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-constructable-stylesheets =
    .label = CSS : fuèlh d’estil constructibla
experimental-features-css-constructable-stylesheets-description = L’apondon d’un constructor a l’interfàcia <a data-l10n-name="mdn-cssstylesheet">CSSStyleSheet</a> atal coma un ensemble de modificacions ligadas que permeton de crear dirèctament de fuèlhs d’estil novèls sans aver d’apondre aqueles al HTML. Facilita fòrça la creacion de fuèlhs d’estil reütilizables d’utilizar amb lo <a data-l10n-name="mdn-shadowdom">Shadow DOM</a>. Vejatz lo <a data-l10n-name="bugzilla">bug 1520690</a> per mai de detalhs.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-media-session-api =
    .label = Web API : API de session mèdia
experimental-features-media-session-api-description = L’implementacion complèta de l’API Media Session dins { -brand-short-name } es actualament experimentala. Aquesta API es utilizada per personalizar la gestion de las notificacions ligats als mèdias, per gerir los eveniments e las donadas que servisson a presentar una interfàcia utilizaire per la gestion de la lectura multimèdia e per obténer las metadonadas dels fichièrs multimèdias. Vejatz lo <a data-l10n-name="bugzilla">bug 1112032</a> per mai de detalhs.
experimental-features-devtools-color-scheme-simulation =
    .label = Aisinas de desvolopaires : simulacion d’equèma de color
experimental-features-devtools-color-scheme-simulation-description = Ajusta una opcion per simular diferents esquèma de colors per vos permetre de provar lo media queries <a data-l10n-name="mdn-preferscolorscheme">@prefers-color-scheme</a>. En utilizant aqueste media query vòstre fuèlh d’estil pòt s’adaptar a la preferéncia de l’utilizaire per çò de l’interfàcia, siá clara siá escura. Aquesta foncionalitat vos permet d’ensajar vòstre còdi sens aver de modificar los paramètres del navegador (o del sistèma operatiu, se lo navegador sèc la configuracion del sistèma tocant l’esquèma de color preferit). Vejatz lo <a data-l10n-name="bugzilla1">bug 1550804</a> e <a data-l10n-name="bugzilla2">1137699</a> per mai de detalhs.
experimental-features-devtools-execution-context-selector =
    .label = Aisinas de desvolopaires : selector de contèxt d’execucion
experimental-features-devtools-execution-context-selector-description = Aquesta foncionalitat aficha un boton sus la linha de comanda de la consòla per permtre de modificar lo contèxte d’execucion de l’expression picada. Consultatz los <a data-l10n-name="bugzilla1">bug 1605154</a> e <a data-l10n-name="bugzilla2">1605153</a> per mai de detalhs.
experimental-features-devtools-compatibility-panel =
    .label = Aisinas de desvolopaires : panèl de compatibilitat
experimental-features-devtools-compatibility-panel-description = Un panèl lateral per l’inspector de pagina que mòstra informacions detalhadas de l’estat de compatibilitat entre navegadors de vòstra aplicacion. Veire lo <a data-l10n-name="bugzilla">bug 1584464</a> per mai de detalhs.
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-lax-by-default2 =
    .label = Cookies : SameSite=Lax per defaut
experimental-features-cookie-samesite-lax-by-default2-description = Tracta los cookies coma « SameSite=Lax » per defaut se l’atribut « SameSite » es pas especificat. Los desvolopaires devon causir l’estatut quo actual d’utilizacion sens cap de  restriccion en definissent explicitament « SameSite=None ».
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-none-requires-secure2 =
    .label = Cookies : SameSite=None requerís l’atribut secure
experimental-features-cookie-samesite-none-requires-secure2-description = Los cookies amb l’atribut « SameSite=None » devon téner l’atribut secure. Aquesta foncionalitat requerís l’activacion de « Cookies : SameSite=Lax per defaut ».
# about:home should be kept in English, as it refers to the the URI for
# the internal default home page.
experimental-features-abouthome-startup-cache =
    .label = cache d’aviada about:home
experimental-features-abouthome-startup-cache-description = Un cache pel document inicial about:home qu’es cargat cada aviada. La tòca del cache es de melhorar las performanças d’aviada.
experimental-features-print-preview-tab-modal =
    .label = Renovacion de l’apercebut abans impression
experimental-features-print-preview-tab-modal-description = Activa un apercebut abans impression novèlament pensat e ara disponible sus macOS. Aquò pòt menar de potencials problèmas e inclutz totes los paramètres ligats a l’impression. Per accedir a totes los paramètres ligats a l’impression, seleccionatz «  Imprimir en utilizant los menús del sistèma… » al panèl d’impression.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-cookie-samesite-schemeful =
    .label = Cookies : Schemeful SameSite
experimental-features-cookie-samesite-schemeful-description = Tracta los cookies del meteis domeni mas amb d’esquèmas diferents (per exemple http://example.com e https://example.com) coma venent de sites diferents allòc d’un meteis site. Melhora la seguretat, mas còpa potencialament de causas.
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support =
    .label = Aisinas de desvolopament : debuggatge del service worker
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support-description = Activa la presa en carga experimentala dels Service workers dins lo panèl de desbugatge. Aquesta foncionalitat pòt alentar las aisinas de desvolopament e aumentar la consomacion de memòria.
# Desktop zooming experiment
experimental-features-graphics-desktop-zooming =
    .label = Grafics : Smooth Pinch Zoom
experimental-features-graphics-desktop-zooming-description = Activa la presa en carga d’un zoom fluid per pinçament suls ecran tactils e los pavats tactils de precesion.
# WebRTC global mute toggle controls
experimental-features-webrtc-global-mute-toggles =
    .label = Interruptor global del son WebRTC
experimental-features-webrtc-global-mute-toggles-description = Ajusta de comandas a l’indicator de partiment global WebRTC que permeton als utilizaires de desactivar lor microfòn e los flux de la camèra.
# JS JIT Warp project
experimental-features-js-warp =
    .label = JavaScript JIT : Warp
experimental-features-js-warp-description = Activa Warp, un projècte per melhorar las qualitats tecnicas de JavaScript e l’utilizacion memòria.
# Fission is the name of the feature and should not be translated.
experimental-features-fission =
    .label = Fission (Isolacion de site)
experimental-features-fission-description = FIssion (isolacion de sites) es una foncionalitat experimentala de { -brand-short-name } per fornir un nivèl suplementari de defensa contra las avarias de seguretat. En isolant cada site dins un processús separat, Fission fa venir mai dur l’accès a l’informacion d’autras paginas que visitatz pels sites malvolents. Aquò es un cambiament d’architectura màger dins { -brand-short-name } e apreciam qu’ensagetz e senhaletz los problèmas que poiriatz rencontrar. Per mai de detalhs, vejatz <a data-l10n-name="wiki">lo wiki</a>.
