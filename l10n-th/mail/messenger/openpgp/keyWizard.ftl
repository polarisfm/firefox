# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#   $identity (String) - the email address of the currently selected identity
key-wizard-dialog-window =
    .title = เพิ่มคีย์ OpenPGP ส่วนตัวสำหรับ { $identity }
key-wizard-button =
    .buttonlabelaccept = ดำเนินการต่อ
    .buttonlabelhelp = กลับไป
key-wizard-warning = <b>หากคุณมีคีย์ส่วนตัว</b>สำหรับที่อยู่อีเมลนี้อยู่แล้ว คุณควรนำเข้า มิฉะนั้นคุณจะไม่สามารถเข้าถึงที่เก็บถาวรอีเมลที่เข้ารหัสของคุณ และไม่สามารถอ่านอีเมลที่เข้ารหัสขาเข้าจากผู้คนที่ยังใช้คีย์ที่มีอยู่ของคุณได้
key-wizard-learn-more = เรียนรู้เพิ่มเติม
radio-create-key =
    .label = สร้างคีย์ OpenPGP ใหม่
    .accesskey = ส
radio-import-key =
    .label = นำเข้าคีย์ OpenPGP ที่มีอยู่
    .accesskey = น
radio-gnupg-key =
    .label = ใช้คีย์ภายนอกของคุณผ่าน GnuPG (เช่น จากสมาร์ทการ์ด)
    .accesskey = ช

## Generate key section

openpgp-generate-key-title = สร้างคีย์ OpenPGP

## Import Key section


## External Key section

