# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-masonry2 =
    .label = CSS: Masonry Layout
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-gpu2 =
    .label = Web API: WebGPU
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-media-avif =
    .label = Media: AVIF
experimental-features-media-avif-description = Kapag naka-enable ang feature na ito, susuportahan ng { -brand-short-name } ang AV1 Image File (AVIF) format. Ito ay isang uri ng larawan na gumagamit sa kapasidad ng AV1 video compression algorithm para mabawasan ang laki ng larawan. Tingnan ang <a data-l10n-name="bugzilla">bug 1443863</a> para sa karagdagang detalye.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-inputmode =
    .label = Web API: inputmode
# "inputmode" and "contenteditable" are technical terms and shouldn't be translated.
experimental-features-web-api-inputmode-description = Ang aming implementasyon ng <a data-l10n-name="mdn-inputmode">inputmode</a> global attribute ay na-update na ayon sa <a data-l10n-name="whatwg">WHATWG specification</a>, ngunit kailangan pa rin naming gumawa ng ibang pagbabago, kagaya ng magawa itong pwedeng gamitin sa contenteditable content. Tingnan ang <a data-l10n-name="bugzilla">bug 1205133</a> para sa karagdagang detalye.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-link-preload =
    .label = Web API: <link rel="preload">
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-focus-visible =
    .label = CSS: Pseudo-class: :focus-visible
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-beforeinput =
    .label = Web API: beforeinput Event
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-constructable-stylesheets =
    .label = CSS: Constructable Stylesheets
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-media-session-api =
    .label = Web API: Media Session API
experimental-features-media-session-api-description = Ang buong implementasyon ng { -brand-short-name } sa Media Session API ay kasalukuyang eksperimental. Ang API na ito ay ginagamit para mapasadya ang paghawak sa mga media-related notification, ma-manage ang mga event at data na maaaring gamitin sa pagpapakita ng user interface sa pag-manage ng media playback, at para makakuha ng media file metadata. Tingnan ang <a data-l10n-name="bugzilla">bug 1112032</a> para sa karagdagang detalye.
experimental-features-devtools-color-scheme-simulation =
    .label = Developer Tools: Color Scheme Simulation
experimental-features-devtools-execution-context-selector =
    .label = Developer Tools: Execution Context Selector
experimental-features-devtools-compatibility-panel =
    .label = Developer Tools: Compatibility Panel
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-lax-by-default2 =
    .label = Cookies: SameSite=Lax by default
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-none-requires-secure2 =
    .label = Mga Cookie: Nangangailangan ng secure attribute ang SameSite=None
experimental-features-cookie-samesite-none-requires-secure2-description = Ang mga cookie na may “SameSite=None” attribute ay nangangailangan ng secure attribute. Kinakailangan nito ang “Cookies: SameSite=Lax by default”.
# about:home should be kept in English, as it refers to the the URI for
# the internal default home page.
experimental-features-abouthome-startup-cache =
    .label = about:home startup cache
experimental-features-print-preview-tab-modal =
    .label = Print Preview Redesign
experimental-features-print-preview-tab-modal-description = Ipinakikilala ang ni-redesign na print preview at ginagawang available ang print preview sa macOS. Maaari itong makadulot ng pagkasira at hindi pa isinasama lahat ng mga setting na may kinalaman sa pag-print. Para ma-access lahat ng mga setting na ito, piliin ang “Mag-print gamit ang system dialog…” mula sa loob ng Print panel.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-cookie-samesite-schemeful =
    .label = Cookies: Schemeful SameSite
experimental-features-cookie-samesite-schemeful-description = Tratuhing cross-site ang mga cookie na nanggaling sa kaparehong domain pero magkaiba ng scheme (hal., http://example.com at https://example.com) sa halip na same-site. Napapaigting ang seguridad, ngunit maaaring makadulot ng pagkasira.
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support =
    .label = Developer Tools: Service Worker debugging
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support-description = Nag-e-enable ng eksperimental na suporta para sa mga Service Worker sa Debugger panel. Ang feature na ito ay maaaring makapagpabagal sa Developer Tools at magpataas ng pagkonsumo ng memory.
# Desktop zooming experiment
experimental-features-graphics-desktop-zooming =
    .label = Graphics: Smooth Pinch Zoom
experimental-features-graphics-desktop-zooming-description = Mag-enable ng suporta para sa smooth pinch zooming sa mga touchscreen at precision touch pad.
# WebRTC global mute toggle controls
experimental-features-webrtc-global-mute-toggles =
    .label = WebRTC Global Mute Toggles
# JS JIT Warp project
experimental-features-js-warp =
    .label = JavaScript JIT: Warp
experimental-features-js-warp-description = I-enable ang Warp, isang proyekto para mapaganda ang JavaScript performance at paggamit sa memory.
# Fission is the name of the feature and should not be translated.
experimental-features-fission =
    .label = Fission (Site Isolation)
