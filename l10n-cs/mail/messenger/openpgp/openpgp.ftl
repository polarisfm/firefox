# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

openpgp-key-man-discover-cmd =
    .label = Najít klíče na internetu
    .accesskey = i

## e2e encryption settings

openpgp-add-key-button =
    .label = Přidat klíč…
    .accesskey = a

## OpenPGP Key selection area


## Account settings export output

