# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-masonry2 =
    .label = CSS: Masonry Layout
experimental-features-css-masonry-description = Tatz'ija' ri k'amoj rik'in ri tojtob'enel rusamaj ruwachib'enik mamposteria CSS. Tab'etz'eta' ri <a data-l10n-name="explainer">rutzijol</a> richin nik'ul jun nïm rutzijoxik rusamaj. Richin nitaq rutzijol. Richin nitaq rutzijol, katzijon pa <a data-l10n-name="w3c-issue"> rere' ruk'ayewal GitHub</a> o <a data-l10n-name="bug"> re sacjpk re'</a>.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-gpu2 =
    .label = Web API: WebGPU
experimental-features-web-gpu-description2 = Re k'ak'a' API ko'öl to'ïk nuya' richin niq'ijüx chuqa' rumolsamaj wachib'äl akuchi' nokisäx ri <a data-l10n-name="wikipedia">Graphics Processing Unit (GPU)</a> pa rukematz'ib' o royonib'al okisaxel. Ri <a data-l10n-name="spec">rujikib'axik</a> k'a man jikïl ta. Tab'etz'eta' ri <a data-l10n-name="bugzilla">bug1602129</a> richin ch'aqa' rutzijol.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-media-avif =
    .label = Media: AVIF
experimental-features-media-avif-description = Rik'in re samaj tzijon re', { -brand-short-name } nutob'ej ri rub'anikil ruyakb'al wachib'äl AV1 (AVIF). Re re' jun rub'anikil ruyakb'al wachib'äl ri nikokisab'ej ketamab'al taq algoritmo richin kijitz'oj AV1 richin nich'utinisäx rupalem. Tab'etz'eta' ri <a data-l10n-name="bugzilla">bug 1443863</a> richin ri rub'anikil.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-inputmode =
    .label = Web API: inputmode
# "inputmode" and "contenteditable" are technical terms and shouldn't be translated.
experimental-features-web-api-inputmode-description = Ri qanimirisanem richin ri chijun b'anikil <a data-l10n-name="mdn-inputmode">inputmode</a> xk'ex rik'in <a data-l10n-name="whatwg">ri WHATWG retal</a>, po k'a k'o na chi niqab'än taq jaloj, achi'el chi kek'ojel pa contenteditable content. Titz'et <a data-l10n-name="bugzilla">sachoj 1205133</a> richin ri retal.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-link-preload =
    .label = Web API: <link rel="preload">
# Do not translate "rel", "preload" or "link" here, as they are all HTML spec
# values that do not get translated.
experimental-features-web-api-link-preload-description = Ri <a data-l10n-name="rel">rel</a> b'anikil rik'in rejqalem <code>"preload"</code> pa jun wachinäq <a data-l10n-name="link">link&gt;</a> richin nito'on nutzilaj rub'eyal nisamäj toq nuqasaj taq rutob'al ruxaq, nutz'ët chi k'o chi e k'o richin majun niq'at ri ruwachib'al ruxaq. Tisik'ïx <a data-l10n-name="readmore">“Rusamajixik chik rupam <code> %MOZ_APP_DISPLAYNAME% </code>”</a> o titz'et <a data-l10n-name="bugzilla">sachoj 1583604</a> richin rutzijol.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-focus-visible =
    .label = CSS: Pseudo-class: :focus-visible
experimental-features-css-focus-visible-description = Nuya' q'ij chi kib'anikil jikib'anïk tisamajïx pa kiwi' taq wachinäq achi'el taq pitz'b'äl chuqa' chajinel taq nojwuj, toq nijikib'äx rik'in pitz'b'äl (achi'el toq nijal ruwi' rik'in wachinäq), man ke ta ri' toq nijikib'äx rik'in ri ch'oy o jun chik okisab'äl. Tatz'eta' <a data-l10n-name="bugzilla">bug 1617600</a> richin rutzijol.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-beforeinput =
    .label = Web API: beforeinput Event
# The terms "beforeinput", "input", "textarea", and "contenteditable" are technical terms
# and shouldn't be translated.
experimental-features-web-api-beforeinput-description = Ri chijun nimaq'ij <a data-l10n-name="mdn-beforeinput">beforeinput</a> nitzij pa wachinäq <a data-l10n-name="mdn-input">input&gt;</a> chuqa' <a data-l10n-name="mdn-textarea">&lt;textarea&gt;</a> o xab'achike wachinäq ruk'wan<a data-l10n-name="mdn-contenteditable">contentenditable</a> tzijïl, chuwäch nujäl ri' ri rejqalem wachinäq. Ri samaj nuya' q'ij chi ri ajk'amaya'l chokoy nujäl rub'anikil ri okik'amaya'l richin nisamäj ri okisanel, akuchi' nuya' q'ij chi ri ajk'amaya'l taq chokoy nuq'ät koken okisanela' rik'in xa xe jikïl taq tz'ib' o nuya' q'ij nikitz'ib'aj jikïl taq tz'ib' o nikijäl rujalik tz'ib' rik'in rub'anikil, akuchi' nuya' q'ij  xa xe chi ke ri jikib'an taq b'anikil
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-constructable-stylesheets =
    .label = CSS: Constructable Stylesheets
experimental-features-css-constructable-stylesheets-description = Ri rutz'aqatisaxik jun nuk'unel pa ri <a data-l10n-name="mdn-cssstylesheet">CSSStyleSheet</a> k'amal achi'el chuqa' juley taq jaloj kik'amon ki' nub'än chi jumul yetz'uk k'ak'a' taq ruxaq rik'in kib'anikil akuchi' man nitz'aqatisäx ta ri HTML ruxaq. Re re' nub'än chi man k'ayew ta yetz'uk kib'anikil ruxaq ri ye'okisäx chik rik'in <a data-l10n-name="mdn-shadowdom">Shadow DOM</a>. Titz'et <a data-l10n-name="bugzilla">bug 1520690</a> richin ch'aqa' rutzijol.
experimental-features-devtools-color-scheme-simulation =
    .label = Developer Tools: Color Scheme Simulation
experimental-features-devtools-color-scheme-simulation-description = Titz'aqatisäx jun cha'oj richin yetojtob'ëx jalajöj taq kichib'enik b'onil, nikiya' q'ij chawe richin ye'atz'ét taq q'ojom <a data-l10n-name="mdn-preferscolorscheme">@prefers-color-scheme</a>. Ri rokisaxik re samaj re' nuya' q'ij chi ri ruxaq b'anikil tutzolij pe tzij chi rij we ri okisanel nrajo' jun ruk'amal okisanel q'equm o säq  ruwa. Re re' nuya' q'ij chawe natojtob'ej ri b'itz'ib' akuchi' man najäl ta ri runuk'ulem okik'amaya'l (o ri rusamajel q'inoj we ri okik'amaya'l nrojqaj jun runuk'ulem ruchib'enik b'onil jikib'an richin chijun ri q'inoj). Tatz'eta' <a data-l10n-name="bugzilla1">bug 1550804</a> chuqa' <a data-l10n-name="bugzilla2">bug 1137699</a> richin ch'aqa' retal.
experimental-features-devtools-execution-context-selector =
    .label = Developer Tools: Execution Context Selector
experimental-features-devtools-execution-context-selector-description = Re samaj nuk'üt jun pitz'b'äl pa ruk'amal rukomanto' konsola richin yatikïr najäl rub'eyal ruk'ojlem xtisamajïx ri tz'ib'anïk xatz'ib'aj, Tatz'eta' <a data-l10n-name="bugzilla1">sachoj 1605154</a> chuqa' <a data-l10n-name="bugzilla2">bug 1605153</a> richin ch'aqa' retal.
experimental-features-devtools-compatibility-panel =
    .label = Developer Tools: Compatibility Panel
experimental-features-devtools-compatibility-panel-description = Jun ruchi' pas richin ri Runik'onel Ruxaq ri nuk'üt pe ri etamab'äl akuchi' nuya' retal rub'anikil k'amoj chi kikojol ri okik'amaya' richin ri achokoy. Tatz'eta' <a data-l10n-name="bugzilla">bug 1584464</a> richin ch'aqa' rutzijol.
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-lax-by-default2 =
    .label = Cookies: SameSite=Lax k'o wi
experimental-features-cookie-samesite-lax-by-default2-description = Yerutz'ët ri taq cookies achi'el “SameSite=Lax” ri k'o wi we man nijikib'äx ta ri “SameSite” b'anikil. Ri b'anonela' k'o chi nikojqaj ri status quo k'o wakami toq nub'ij “SameSite=None”.
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-none-requires-secure2 =
    .label = Cookies: SameSite=None nrajo' jun jikïl b'anikil
experimental-features-cookie-samesite-none-requires-secure2-description = Taq cookies rik'in “SameSite=None” nrajo' ri jikïl b'anikil. Re b'anikil re' nrajo' “Cookies: SameSite=Lax k'o wi”.
# about:home should be kept in English, as it refers to the the URI for
# the internal default home page.
experimental-features-abouthome-startup-cache =
    .label = about:home rujumejyak tikirib'äl
experimental-features-abouthome-startup-cache-description = Rujumejyak tikirinel wuj  about:home ri nusamajij ri' ruyon pa rutikiribal. Ri nrojqaj ri jumejyak richin ninimirisäx rusamaj ri tikirib'äl.
experimental-features-print-preview-tab-modal =
    .label = Ruwachib'exik Chik Nab'ey Rutz'etoj Tz'ajb'anïk
experimental-features-print-preview-tab-modal-description = Nuk'üt ri nab'ey rutz'etoj tz'ajb'anïk xwachib'ëx chik chuqa' nub'än chi ri tz'ajb'anïk k'o pa macOS. Nuq'alajisaj nima'q taq sachoj chuqa' man ruk'wan ta ronojel runuk'ulem pa ruwi' rutz'ajb'axik. Richin yatol pa runuk'ulem tz'ajb'anïk tacha' “Titz'ajb'äx rik'in rutzijol q'inoj…”  pa rupas Tz'ajb'anïk.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-cookie-samesite-schemeful =
    .label = Cookies: Schemeful SameSite
experimental-features-cookie-samesite-schemeful-description = Kisamajixik taq cookies richin junam ajk'amal po rik'in jalajöj chib'enïk (achi'el, http://example.com chuqa' https://example.com) achi'el xoch'in taq ruxaq pa ruk'exel ri jun ruxaq. Nrutzilaj ri jikomal po nuya' k'ïy sachoj.
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support =
    .label = Developer Tools: Service Worker debugging
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support-description = Nutzïj ri tojtob'enel tob'äl richin Service Workers pa Rupas Chojmirisanel. Re rub'anikil re' nub'än eqal chi ke ri Kisamajib'al B'anonela' chuqa' yalan uchuqa' nrokisaj.
# Desktop zooming experiment
experimental-features-graphics-desktop-zooming =
    .label = Graphics: Smooth Pinch Zoom
experimental-features-graphics-desktop-zooming-description = Nitzij ri tob'äl richin eqal ninimirisäx toq nachäp ri chapel taq ruwa chuqa' ajchapöy pads.
# WebRTC global mute toggle controls
experimental-features-webrtc-global-mute-toggles =
    .label = WebRTC Chijun K'oxom Silowäch
experimental-features-webrtc-global-mute-toggles-description = Ketz'aqatisäx taq samajib'äl pa ri WebRTC chijun komonïk etanel ri nikiya' q'ij chi ke ri okisanela' yekichüp chijun ri ketal q'asäy taq kich'ab'äl chuqa' elesäy wachib'äl.
# JS JIT Warp project
experimental-features-js-warp =
    .label = JavaScript JIT: Warp
experimental-features-js-warp-description = Titzij Warp, jun nuk'samaj richin nutziläx ri rub'eyal nisamäj chuqa' rokisaxik ruchuq'a' JavaScript.
# Fission is the name of the feature and should not be translated.
experimental-features-fission =
    .label = Fission (Rujech'unik Ruxaq)
experimental-features-fission-description = Fission (rujech'unik ruxaq) jun tojtob'enel samaj pa richin jun rutz'aqat pisoj richin yeruchajij taq rusachoj jikomal. Toq yerujech'uj jujun ruxaq pa kiyonil, Fission k'ayew nub'än chi ke ri itzel taq ajk'amaya'l ruxaq richin ye'ok pa ketamab'al ch'aqa' chik taq ruxaq ye'atz'ët. Re re' jun nimaläj tz'aqonel jaloj pa { -brand-short-name } chuqa' niqamatyoxij chawe chi natojtob'ej chuqa' naya' rutzijol xab'achike k'ayewal nawïl. Richin nawetamaj ch'aqa' chik rub'anikil, tatz'eta' <a data-l10n-name="wiki">ri wiki</a>.
