# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

key-wizard-button =
    .buttonlabelaccept = Pokračovať
    .buttonlabelhelp = Späť
key-wizard-learn-more = Ďalšie informácie

## Generate key section

openpgp-keygen-type-rsa =
    .label = RSA
openpgp-keygen-confirm =
    .label = Potvrdiť
openpgp-keygen-dismiss =
    .label = Zrušiť
openpgp-keygen-cancel =
    .label = Zrušiť proces…
openpgp-keygen-import-complete =
    .label = Zavrieť
    .accesskey = Z

## Import Key section

openpgp-import-identity-label = Identita
openpgp-import-fingerprint-label = Odtlačok prsta

## External Key section

openpgp-external-key-input =
    .placeholder = 123456789341298340
